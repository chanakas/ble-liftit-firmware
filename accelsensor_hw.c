/******************************************************************************
 *  FILE
 *      accelsensor_hw.c
 *
 *  DESCRIPTION
 *      This file implements the abstraction for temperature sensor hardware
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <pio.h>
#include <timer.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "user_config.h"
#include "accelsensor_hw.h"
#ifdef ACCELEROMETER_SENSOR_BMA250
#include "bma250_accelerometer_sensor.h"
#endif /* ACCELEROMETER_SENSOR_BMA250 */

/*============================================================================*
 *  Private data
 *============================================================================*/


/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 *  NAME
 *      AccelSensorHardwareInit
 *
 *  DESCRIPTION
 *      This function initialises the accelerometer sensor hardware.
 *
 *  RETURNS
 *      TRUE if initialization is sucessful.
 *
 *----------------------------------------------------------------------------*/
extern bool AccelSensorHardwareInit(void)
{
    bool status = FALSE;
#ifdef ACCELEROMETER_SENSOR_BMA250
    status = BMA250_Init();
#endif             
    return status;
}

