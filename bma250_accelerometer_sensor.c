/******************************************************************************
 *
 *  FILE
 *      bma250_accelerometer_sensor.c
 *
 *  DESCRIPTION
 *      This file implements the procedure for communicating with a BMA250
 *      Sensor.
 *
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <types.h>
#include <macros.h>
#include <timer.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "user_config.h"
#include "bma250_accelerometer_sensor.h"
#include "i2c_comms.h"

/*============================================================================*
 *  Private data
 *============================================================================*/


/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/


#ifdef ACCELEROMETER_SENSOR_BMA250
/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/
/*----------------------------------------------------------------------------*
 *  NAME
 *      BMA250_Init
 *
 *  DESCRIPTION
 *      This function initialises the accelerometer sensor BMA250
 *
 *  RETURNS
 *      TRUE if success.
 *
 *---------------------------------------------------------------------------*/
extern bool BMA250_Init(void)
{
    uint8 manu_id = 1;
    bool  success = FALSE;

    /* Acquire I2C bus */
    if(I2CAcquire())
    {
        /* Initialise I2C communication. */
        I2CcommsInit();

        /* Read the Manufacturer's ID and check */
        success = I2CReadRegister(0x30, 0x00, &manu_id);
        if ((!success) || (BMA250_MANU_ID != manu_id))
        {
            success = FALSE;
        }
        else
        {
            /* Configure the accelerometer sensor */
            
            /* Software reset */
            /*success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_SOFTRESET, 
                                       0xB6);*/
            /* Setup Bandwidth 1000Hz */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_BANDWIDTH, 
                                       0x0F);
            /* Setup Range  +-2g */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_RANGE, 
                                       0x03);
            /* Map Slope/Movement to INT1 */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_INT_MAP_0, 
                                       0x04);
            
            /* Map Flat INT to INT2 */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_INT_MAP_2, 
                                       0x80);
            
            /* Select Filtered Data */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_INT_SRC, 
                                       0x00);
            /* INT Pin Push-Pull, Active Low */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_INT_OUT_CTR, 
                                       0x00);
            /* INT Duration 50ms*/
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_INT_RST_LATCH, 
                                       0x0E);
            /* Slope Duration 1 samples */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, 0x27, 
                                       0x00);
            /* Slope Threshold */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, 0x28, 
                                       0x04);
            
            /* Flat Theta */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, 0x2E, 
                                       0x04);
            
            /* Flat Hold Time */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, 0x2F, 
                                       0x20);   // 1024ms
            
            /* Enable Slope (X,Y,Z axis) and Flat Interrupt */
            //success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_INT_EN_0, 
            //                           0x87);              
            
            /* Setup LPM1 */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, 0x12, 
                                       0x00);   
            
            /* Setup Low Power Mode */
            success = I2CWriteRegister(BMA250_I2C_ADDRESS, REG_LOWPOWERMODE, 
                                       0x5C);   // 500ms sleep
            
        }

        /* Release the I2C bus */
        I2CRelease();
    }
    
    return success;
}

extern void BMA250_InterruptHandler(void)
{
}

#endif /* ACCELEROMETER_SENSOR_BMA250 */

