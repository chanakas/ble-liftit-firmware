/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2013-2014
 *  Part of CSR uEnergy SDK 2.4.3
 *  Application version 2.4.3.0
 *
 *  FILE
 *      hw_access.c
 *
 *  DESCRIPTION
 *      This file defines the application hardware specific routines.
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <pio.h>            /* PIO configuration and control functions */
#include <pio_ctrlr.h>      /* Access to the PIO controller */
#include <timer.h>          /* Chip timer functions */

/*============================================================================*
 *  Local Header Files
 *============================================================================*/

#include "hw_access.h"      /* Interface to this file */
#include "beacon.h"    /* Definitions used throughout the GATT server */

/*============================================================================*
 *  Private Definitions
 *============================================================================*/

/* Setup PIO 11 as Button PIO */
#define BUTTON_PIO                  (0)

#define BUTTON_PIO_MASK             (PIO_BIT_MASK(BUTTON_PIO))

/* Extra long button press timer */
#define EXTRA_LONG_BUTTON_PRESS_TIMER \
                                    (1*SECOND)
                                            
#define KEY_PRESSED  (TRUE)
#define KEY_RELEASED (FALSE)

/*============================================================================*
 *  Private Data
 *============================================================================*/

/* Switch Button States. */
static bool     buttonState  = KEY_RELEASED;
static bool     int1State  = KEY_RELEASED;
static bool     int2State  = KEY_RELEASED;
                                            
                                            
/*============================================================================*
 *  Public data type
 *============================================================================*/

/* Application Hardware data structure */
typedef struct _APP_HW_DATA_T
{

    /* Timer for button press */
    timer_id                    button_press_tid;

} APP_HW_DATA_T;

/*============================================================================*
 *  Public data
 *============================================================================*/

/* Application hardware data instance */
static APP_HW_DATA_T            g_app_hw_data;

/*============================================================================*
 *  Private Function Prototypes
 *============================================================================*/
static void handleExtraLongButtonPress(timer_id tid);
static void HandleShortButtonPress(void);


static void handleExtraLongButtonPress(timer_id tid)
{
        if (g_app_hw_data.button_press_tid == tid)
    {
        togglePower(0);
        g_app_hw_data.button_press_tid = TIMER_INVALID;
    }
}

static void HandleShortButtonPress(void)
{

        togglePower(1);
     
}

/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 *  NAME
 *      IOTLightControlDeviceInit
 *
 *  DESCRIPTION
 *      This function initialises the Red, Green and Blue LED lines.
 *      Configure the IO lines connected to Switch as inputs.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void IOTLightControlDeviceInit(void)
{
    /* Configure the LED PIO as output PIO */
    PioSetDir(LED_PIO, PIO_DIRECTION_OUTPUT);
    PioSetMode(LED_PIO, pio_mode_user);
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      IOTLightControlDevicePower
 *
 *  DESCRIPTION
 *      This function sets power state of LED.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void IOTLightControlDevicePower(bool power_on)
{
    if (power_on == TRUE)
    {
        PioSet(LED_PIO, 1);
    }
    else
    {
        PioSet(LED_PIO, 0);
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      IOTLightBlink
 *
 *  DESCRIPTION
 *      This function sets LED to blink.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void IOTLightBlink(void)
{
    /* Connect PWM0 output to LED_PIO_RED */
    PioSetMode(LED_PIO, pio_mode_pwm0);

    /* Enable the PIO's */
    PioEnablePWM(LED_PWM, TRUE);
    
    PioConfigPWM(LED_PWM, pio_pwm_mode_push_pull,
                 0, 255, 255, 100, 50, 1, 0U);
}    

/*----------------------------------------------------------------------------*
 *  NAME
 *      InitHardware
 *
 *  DESCRIPTION
 *      This function is called to initialise the application hardware.
 *
 *  PARAMETERS
 *      None
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
extern void InitHardware(void)
{
    /* Set-up the PIOs for the switches */
    PioSetDir(PUSH_SW_PIO, PIO_DIRECTION_INPUT);
    PioSetMode(PUSH_SW_PIO, pio_mode_user);

    PioSetDir(INT2_PIO, PIO_DIRECTION_INPUT);
    PioSetMode(INT2_PIO, pio_mode_user);

    PioSetDir(INT1_PIO, PIO_DIRECTION_INPUT);
    PioSetMode(INT1_PIO, pio_mode_user);
    
    PioSetPullModes(ALL_BIT_MASK , pio_mode_strong_pull_up);
    PioSetEventMask(BUTTON_BIT_MASK , pio_event_mode_both); 
    
    IOTLightControlDeviceInit();
    
    /* Switch off the light */
    IOTLightControlDevicePower(FALSE);

    /* Read the satus of the PUSH SW PIO and set the position accordingly */  
    if(PioGet(INT1_PIO) == FALSE)
    {
        int1State = KEY_PRESSED;
    }
    
    if(PioGet(INT2_PIO) == FALSE)
    {
        int2State = KEY_PRESSED;
    }
    
    if(PioGet(PUSH_SW_PIO) == FALSE)
    {
        buttonState = KEY_PRESSED;
    }
    
    /* Setup PIOs
     * PIO11 - Button
     */
    /* Set the button PIO to user mode */
    PioSetModes(BUTTON_PIO_MASK, pio_mode_user);

    /* Set the PIO direction as input */
    PioSetDir(BUTTON_PIO, PIO_DIRECTION_INPUT);

    /* Pull up the PIO */
    PioSetPullModes(BUTTON_PIO_MASK, pio_mode_strong_pull_up);

    /* Request an event when the button PIO changes state */
    PioSetEventMask(BUTTON_PIO_MASK, pio_event_mode_both);

    /* Save power by changing the I2C pull mode to pull down.*/
    //PioSetI2CPullMode(pio_i2c_pull_mode_strong_pull_down);
    
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      HwDataInit
 *
 *  DESCRIPTION
 *      This function initialises the hardware data to a known state. It is
 *      intended to be called once, for example after a power-on reset or HCI
 *      reset.
 *
 *  PARAMETERS
 *      None
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
extern void HwDataInit(void)
{
    /* Initialise button press timer */
    g_app_hw_data.button_press_tid = TIMER_INVALID;

}

/*----------------------------------------------------------------------------*
 *  NAME
 *      HwDataReset
 *
 *  DESCRIPTION
 *      This function resets the hardware data. It is intended to be called when
 *      the data needs to be reset to a clean state, for example, whenever a
 *      device connects or disconnects.
 *
 *  PARAMETERS
 *      None
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
extern void HwDataReset(void)
{
    /* Delete button press timer */
    if (g_app_hw_data.button_press_tid != TIMER_INVALID)
    {
        TimerDelete(g_app_hw_data.button_press_tid);
        g_app_hw_data.button_press_tid = TIMER_INVALID;
    }
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      HandlePIOChangedEvent
 *
 *  DESCRIPTION
 *      This function handles the PIO Changed event.
 *
 *  PARAMETERS
 *      pio_data [in]           State of the PIOs when the event occurred
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
extern void HandlePIOChangedEvent(uint32 pio_changed)
{

    if(pio_changed & PUSH_SW_MASK)
    {
        /* PIO changed */
        uint32 pios = PioGets();
        if(!(pios &PUSH_SW_MASK))
        {
            /* This event is triggered when a button is pressed. */

            /* Start a timer for EXTRA_LONG_BUTTON_PRESS_TIMER seconds. If the
             * timer expires before the button is released an extra long button
             * press is detected. If the button is released before the timer
             * expires a short button press is detected.
             */
            TimerDelete(g_app_hw_data.button_press_tid);

            g_app_hw_data.button_press_tid = 
                TimerCreate(EXTRA_LONG_BUTTON_PRESS_TIMER,
                                          TRUE, handleExtraLongButtonPress);
        }
        else
        {
            /* This event comes when a button is released. */
                /* Timer was already running. This means it was a short button 
                 * press.
                 */
                TimerDelete(g_app_hw_data.button_press_tid);
                g_app_hw_data.button_press_tid = TIMER_INVALID;

               HandleShortButtonPress();
            
        }
    }
}

