/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited, 2014
 *
 *  FILE
 *      beacon.c
 *
 *  DESCRIPTION
 *      This file defines an advertising node implementation
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <main.h>
#include <gap_app_if.h>
#include <config_store.h>
#include <pio.h>
#include <random.h>
#include <types.h>          /* Commonly used type definitions */
#include <timer.h>          /* Chip timer functions */
#include <panic.h>          /* Support for applications to panic */
#include <battery.h>        /* Read the battery voltage */

/*============================================================================*
 *  Local Header File
 *============================================================================*/

#include "beacon.h"
#include "hw_access.h"      /* Hardware access */
#include "accelsensor_hw.h"
#include "i2c_comms.h"
#include "user_config.h"

/*============================================================================*
 *  Private Definitions
 *============================================================================*/

#define ACCEL_READ_INTERVAL     (1.0 * SECOND)
#define TRIGGER_THRESHOLD       20
#define BEACON_TIMEOUT          (2.0 * SECOND)
#define BEACON_INTERVAL         100  /* in milliseconds */


#define MAX_APP_TIMERS          (10)

/* Battery full level as a percentage */
#define BATTERY_LEVEL_FULL                            (100)

/* Battery critical level as a percentage */
#define BATTERY_CRITICAL_LEVEL                        (10)

/* Battery minimum and maximum voltages in mV */
#define BATTERY_FULL_BATTERY_VOLTAGE                  (3000)          /* 3.0V */
#define BATTERY_FLAT_BATTERY_VOLTAGE                  (1800)          /* 1.8V */


/*============================================================================*
 *  Private Data
 *============================================================================*/

/* Declare space for application timers */
static uint16 app_timers[SIZEOF_APP_TIMER * MAX_APP_TIMERS];
uint8 tempX1, tempY1, tempZ1;
uint8 tempX2, tempY2, tempZ2;
uint8 diffX, diffY, diffZ;
uint8 accX1, accX2;
uint8 accY1, accY2;
uint8 accZ1, accZ2;
int restCount = 0;
//static int isBroadcasting = 0;
static uint8 seed = 2;
uint8 sensorTrigger = 0;
uint8 prevState = 0;
static timer_id beacon_timer_tid;

uint8 batteryPercent;

/*============================================================================*
 *  Private Function Prototypes
 *============================================================================*/

static void setupAdvertising(void);

static void startAdvertising(uint8 sensorValue);

static void appSetRandomAddress(void);

/* Start timer */


/* Callback after first timeout */


static void beaconTimerInt(timer_id const id);
        

/* Read the battery level */
static uint8 readBatteryLevel(void);

/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 *  NAME
 *      appSetRandomAddress
 *
 *  DESCRIPTION
 *      This function generates a non-resolvable private address and sets it
 *      to the firmware.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void appSetRandomAddress(void)
{
    BD_ADDR_T addr;

    /* "completely" random MAC addresses by default: */
    for(;;)
    {
        uint32 now = TimeGet32();
        /* Random32() is just two of them, no use */
        uint32 rnd = Random16();
        addr.uap = 0xff & (rnd ^ now);
        /* No sub-part may be zero or all-1s */
        if ( 0 == addr.uap || 0xff == addr.uap ) continue;
        addr.lap = 0xffffff & ((now >> 8) ^ (73 * rnd));
        if ( 0 == addr.lap || 0xffffff == addr.lap ) continue;
        addr.nap = 0x3fff & rnd;
        if ( 0 == addr.nap || 0x3fff == addr.nap ) continue;
        break;
    }

    /* Set it to actually be an acceptable random address */
    addr.nap &= ~BD_ADDR_NAP_RANDOM_TYPE_MASK;
    addr.nap |=  BD_ADDR_NAP_RANDOM_TYPE_NONRESOLV;
    GapSetRandomAddress(&addr);
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      setupAdvertising
 *
 *  DESCRIPTION
 *      This function is called to setup advertisements.
 *
 *      Advertisement packet will contain Flags AD and Manufacturer-specific
 *      AD with Manufacturer id set to CSR and payload set to the value of
 *      the User Key 0. The payload size is set by the User Key 1.
 *
 *      +--------+-------------------------------------------------+
 *      |FLAGS AD|MANUFACTURER AD                                  |
 *      +--------+-------------------------------------------------+
 *       0      2 3
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
void setupAdvertising(void)
{
    uint8 advData[MAX_ADVERT_PACKET_SIZE];
    uint16 offset = 0;
    uint8 filler;
    uint16 advInterval;
    uint8 advPayloadSize;
    ls_addr_type addressType = ls_addr_type_public;     /* use public address */
    
    /* initialise values from User CsKeys */
    
    /* read User key 0 for the payload filler */
    filler = (uint8)(CSReadUserKey(0) & 0x00FF);
    
    /* read User key 1 for the payload size */
    advPayloadSize = (uint8)(CSReadUserKey(1) & 0x00FF);
    
    /* range check */
    if((advPayloadSize < 1) || (advPayloadSize > MAX_ADVERT_PAYLOAD_SIZE))
    {
        /* revert to default payload size */
        advPayloadSize = DEFAULT_ADVERT_PAYLOAD_SIZE;
    }  
   
    advInterval = BEACON_INTERVAL;
               
    /* read address type from User key 3 */
    if(CSReadUserKey(3))
    {
        /* use random address type */
        addressType = ls_addr_type_random;

        /* generate and set the random address */
        appSetRandomAddress();
    }

    /* set the GAP Broadcaster role */
    GapSetMode(gap_role_broadcaster,
               gap_mode_discover_no,
               gap_mode_connect_no,
               gap_mode_bond_no,
               gap_mode_security_none);
    
    /* clear the existing advertisement data, if any */
    LsStoreAdvScanData(0, NULL, ad_src_advertise);

    /* set the advertisement interval, API accepts the value in microseconds */
    GapSetAdvInterval(advInterval * MILLISECOND, advInterval * MILLISECOND);
    
    /* manufacturer-specific data */
    advData[0] = AD_TYPE_MANUF;

    /* CSR company code, little endian */
    advData[1] = 0x0A;
    advData[2] = 0x00;
    
    /* fill in the rest of the advertisement */
    for(offset = 0; offset < advPayloadSize; offset++)
    {
        advData[3 + offset] = filler;
    }

    /* store the advertisement data */
    LsStoreAdvScanData(advPayloadSize + 3, advData, ad_src_advertise);
    
    /* Start broadcasting */
    //LsStartStopAdvertise(TRUE, whitelist_disabled, addressType);
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      startpAdvertising
 *
 *  DESCRIPTION
 *      This function is called to start advertisements.
 *
 *      Advertisement packet will contain Flags AD and Manufacturer-specific
 *      AD with Manufacturer id set to CSR and payload set to the value of
 *      the User Key 0. The payload size is set by the User Key 1.
 *
 *      +--------+-------------------------------------------------+
 *      |FLAGS AD|MANUFACTURER AD                                  |
 *      +--------+-------------------------------------------------+
 *       0      2 3
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
void startAdvertising(uint8 sensorValue)
{
    uint8 advData[MAX_ADVERT_PACKET_SIZE];
    uint16 offset = 0;
    uint8 filler;
    uint8 advPayloadSize;
    ls_addr_type addressType = ls_addr_type_public;     /* use public address */
    
    filler = 0;
    
    /* read User key 1 for the payload size */
    advPayloadSize = (uint8)(CSReadUserKey(1) & 0x00FF);
    
    /* range check */
    if((advPayloadSize < 1) || (advPayloadSize > MAX_ADVERT_PAYLOAD_SIZE))
    {
        /* revert to default payload size */
        advPayloadSize = DEFAULT_ADVERT_PAYLOAD_SIZE;
    }
        
    /* clear the existing advertisement data, if any */
    LsStoreAdvScanData(0, NULL, ad_src_advertise);
   
    /* manufacturer-specific data */
    advData[0] = AD_TYPE_MANUF;

    /* CSR company code, little endian */
    advData[1] = PAY_LOAD_PREFIX_1;
    advData[2] = PAY_LOAD_PREFIX_2;
    advData[3] = PAY_LOAD_PREFIX_3;
    advData[4] = PAY_LOAD_PREFIX_4;
    advData[5] = PAY_LOAD_PREFIX_5;
    advData[6] = PAY_LOAD_PREFIX_6;
    advData[7] = PAY_LOAD_PREFIX_7;
    advData[8] = PAY_LOAD_PREFIX_8;
    advData[9] = (uint8)((CSReadUserKey(3) & 0xFF00)>>8);
    advData[10] = (uint8)(CSReadUserKey(3) & 0x00FF);
    
    
    advData[11] =  sensorValue;
    
    advData[12] = readBatteryLevel();
    uint8 i = 0;
    
    for(i =1; i < 13; i++){
      advData[i] = advData[i] ^ seed;
    }
    
    advData[13] = seed;
    advData[14] = (uint8)(CSReadUserKey(4) & 0x00FF);
    seed = (uint8)((seed*7) % 251);
    /* fill in the rest of the advertisement */
    for(offset = 15; offset < advPayloadSize; offset++)
    {
        advData[offset] = filler;
    }

    /* store the advertisement data */
    LsStoreAdvScanData(advPayloadSize + 3, advData, ad_src_advertise);
    
    /* Start broadcasting */
    LsStartStopAdvertise(TRUE, whitelist_disabled, addressType);
}



/*----------------------------------------------------------------------------*
 *  NAME
 *      startTimer
 *
 *  DESCRIPTION
 *      Start a timer
 *
 * PARAMETERS
 *      timeout [in]    Timeout period in seconds
 *      handler [in]    Callback handler for when timer expires
 *
 * RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*
 *  NAME
 *      togglePower
 *
 *  DESCRIPTION
 *      This function is called by the long button press event to switch the sensor on and off
 *
 * PARAMETERS
 *      Nothing
 *
 * RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/

extern void togglePower(int state)
{
   if(state == 1){
       IOTLightControlDevicePower(TRUE);
       TimerDelete(beacon_timer_tid);
       beacon_timer_tid = TimerCreate(BEACON_TIMEOUT, TRUE, beaconTimerInt);
       startAdvertising(0xAA);
   }
   else{
       IOTLightControlDevicePower(TRUE);
       TimerDelete(beacon_timer_tid);
       beacon_timer_tid = TimerCreate(BEACON_TIMEOUT, TRUE, beaconTimerInt);
       startAdvertising(0xBB);
  }

  
}
/*----------------------------------------------------------------------------*
 *  NAME
 *      beaconTimerInt
 *
 *  DESCRIPTION
 *      This function is called when the timer created by TimerCreate expires.
 *
 * PARAMETERS
 *      id [in]     ID of timer that has expired
 *
 * RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
static void beaconTimerInt(timer_id const id)
{
    /* Delete Timer */
    TimerDelete(beacon_timer_tid);
    IOTLightControlDevicePower(FALSE);
    /* Stop broadcasting */
    LsStartStopAdvertise(FALSE, whitelist_disabled, ls_addr_type_public);
}



/*----------------------------------------------------------------------------*
 *  NAME
 *      readBatteryLevel
 *
 *  DESCRIPTION
 *      This function reads the battery level.
 *
 *  PARAMETERS
 *      None
 *
 *  RETURNS
 *      Battery level in percent
 *----------------------------------------------------------------------------*/
static uint8 readBatteryLevel(void)
{
    uint32 bat_voltage;                 /* Battery voltage in mV */
    uint32 bat_level;                   /* Battery level in percent */

    /* Read battery voltage and level it with minimum voltage */
    bat_voltage = BatteryReadVoltage();

    /* Level the read battery voltage to the minimum value */
    if(bat_voltage < BATTERY_FLAT_BATTERY_VOLTAGE)
    {
        bat_voltage = BATTERY_FLAT_BATTERY_VOLTAGE;
    }

    bat_voltage -= BATTERY_FLAT_BATTERY_VOLTAGE;

    /* Get battery level in percent */
    bat_level = (bat_voltage * 100) / (BATTERY_FULL_BATTERY_VOLTAGE - 
                                                  BATTERY_FLAT_BATTERY_VOLTAGE);

    /* Check the precision errors */
    if(bat_level > 100)
    {
        bat_level = 100;
    }

    /* Return the battery level (as a percentage of full) */
    return (uint8)bat_level;
}

/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppPowerOnReset
 *
 *  DESCRIPTION
 *      This function is called just after a power-on reset (including after
 *      a firmware panic).
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

void AppPowerOnReset(void)
{
    /* empty */
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppInit
 *
 *  DESCRIPTION
 *      This function is called after a power-on reset (including after a
 *      firmware panic) or after an HCI Reset has been requested.
 *
 *      NOTE: In the case of a power-on reset, this function is called
 *      after AppPowerOnReset().
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

void AppInit(sleep_state last_sleep_state)
{
    /* Initialise the application timers */
    TimerInit(MAX_APP_TIMERS, (void*)app_timers);
    
    /* Initialise local timers */
    beacon_timer_tid = TIMER_INVALID;
    
    /* Initialize hardware */
    InitHardware();
    
    /* Initialize Accelerometer */
    AccelSensorHardwareInit();
       
    /* disable wake up on UART RX */
    SleepWakeOnUartRX(FALSE);

    /* Setup advertising */
    setupAdvertising();
    
    /* Start Timer to read accelerometer */
   // startTimer(ACCEL_READ_INTERVAL, timerCallback1);    
   // accel_timer_tid = TimerCreate(ACCEL_READ_INTERVAL, TRUE, timerCallback1);
    //IOTLightControlDevicePower(TRUE);

}


/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcessSystemEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a system event, such
 *      as a battery low notification, is received by the system.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

void AppProcessSystemEvent(sys_event_id id, void *data)
{
    switch(id)
    {
        case sys_event_pio_changed:
        {
             /* Handle the PIO changed event. */
             HandlePIOChangedEvent(((pio_changed_data*)data)->pio_cause);
        }
        break;
            
        default:
            /* Ignore anything else */
        break;
    }
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcessLmEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a LM-specific event is
 *      received by the system.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

bool AppProcessLmEvent(lm_event_code event_code, 
                       LM_EVENT_T *p_event_data)
{
    return TRUE;
}
