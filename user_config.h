/*******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2013-2014
 *  Part of CSR uEnergy SDK 2.4.3
 *  Application version 2.4.3.0
 *
 * FILE
 *      user_config.h
 *
 * DESCRIPTION
 *      This file contains definitions to customise the application.
 *
 ******************************************************************************/

#ifndef __USER_CONFIG_H__
#define __USER_CONFIG_H__

/*=============================================================================*
 *  Public Definitions
 *============================================================================*/


/* Use Accelerometer */
#define ACCELEROMETER_SENSOR_BMA250

/*Advertising payload structure*/
#define PAY_LOAD_PREFIX_1 0x43;
#define PAY_LOAD_PREFIX_2 0x59;
#define PAY_LOAD_PREFIX_3 0x52;
#define PAY_LOAD_PREFIX_4 0x55;
#define PAY_LOAD_PREFIX_5 0x50;
#define PAY_LOAD_PREFIX_6 0x5F;
#define PAY_LOAD_PREFIX_7 0x53;
#define PAY_LOAD_PREFIX_8 0x45;

/*device id 2 bytes [MSB LSB] 0xff max for each*/
#define DEVICE_ID_MSB 0x00
#define DEVICE_ID_LSB 0x01
#define SENSOR_TYPE 0x02




#endif /* __USER_CONFIG_H__ */
