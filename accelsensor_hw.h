/******************************************************************************
 *  FILE
 *      accelsensor_hw.h
 *
 *  DESCRIPTION
 *      This file defines a interface to abstract hardware specifics of
 *      accelerometer sensor.
 *
 *  NOTES
 *
 ******************************************************************************/
#ifndef __ACCELSENSOR_HW_H__
#define __ACCELSENSOR_HW_H__

/*============================================================================*
 *  Public Definitions
 *============================================================================*/

/* Acceleromter Sensor Hardware Initialization function. */
extern bool AccelSensorHardwareInit(void);

#endif /* __ACCELSENSOR_HW_H__ */
